# TENNIS APP #

Welcome to the tennis app repository. It consist of all files from wireframes... to </code> for the project.  

### How do I get set up? ###

* Download SourceTree 
* Go on clone and select sourcetree option 
* Clone folder can be Desktop or Documents as per your choice 

### Contribution guidelines ###

* Whenver you want to make changes pull first 
* Check first if any of the member has pushed a code. It will be apparent on sourceTree. 
* Both android and web app would be in the tennis app folder as root 

### Devlopers ###

* Anuj Sharma
* Aishwarya Sharam
* Aniketh Nair
* Yash Tibrewal