<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Lawn Tennis </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Sports Category </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li class="current"><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>


 <li><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li><a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                    

                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                            	
                        <!--Default-section-one-->
                        <div class="service-images">
                        	<div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/resource/service-detail-1.jpg" alt="" /></a>
                                    </figure>
                                    
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-2.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/resource/service-detail-2.jpg" alt="" /></a>
                                    </figure>
                                    <figure class="image">
                                        <a href="images/resource/service-detail-3.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/resource/service-detail-3.jpg" alt="" /></a>
                                    </figure>
                                </div>
                             </div>
                        </div>
                        
                        <!--text-content-->
                        <div class="text-content">
                        	<h2>Tennis </h2>
                            <div class="bold-text">The Court shall be a rectangle <span> 78 feet (23.77 m.) long and 27 feet (8. 23 m.) wide</span>. It shall be divided across the middle by a net suspended from a cord or metal cable of a maximum diameter of one-third of an inch (0.8 cm.), the ends of which shall be attached to, or pass over, the tops of the two posts, which shall be not more than 6 inches (15 cm.) square or 6 inches (15 cm.) in diameter. These posts shall not be higher than 1 inch (2.5 cm.) above the top of the net cord. The centers of the post shall be 3 feet (.914 m.) outside the Court on each side and the height of the posts shall be such that the top of the cord or metal cable shall be 3 feet 6 inches (1.07 m.) above the ground.</div>
                            <div class="text">When a combined doubles (see Rule 34) and singles court with a doubles net is used for singles, the net must be supported to a height of 3 feet 6 inches (1.07 m.) by means of two posts, called "singles sticks", which shall be not more than 3 inches (7.5 cm.) square or 3 inches (7.5 cm.) in diameter. The centers of the singles sticks shall be 3 feet (.914 m.) outside the singles Court on each side. The net shall be extended fully so that it fills completely the space between the two posts and shall be of sufficiently small mesh to prevent the ball passing through. The height of the net shall be 3 feet (.914 m.) at the center, where it shall be held down taut by a strap not more than 2 inches (5 cm.) wide and completely white in colour. These shall be a band covering the cord or metal cable and the top of the net of not less than 2 inches (5 cm.) nor more than 2 ½ inches (6. 35 cm.) in depth on each side and completely white in colour. There shall be not advertisement on the net, strap band or singles sticks.</div>
<br>
                             <h2>Our Challanging Part</h2>
                                    <div class="text">
                                        <p>The lines bounding the ends and sides of the Court shall respectively be called the base lines and the sidelines. On each side of the net, at a distance of 21 feet (6.40 m.) from it and parallel with it, shall be drawn the service lines. The space on each side of the net between the service-line and the side-line shall be divided into two equal parts called the service-courts by the center service-line, which must be 2 inches (5 cm.) in width, drawn half-way between, and parallel with the side-line. Each base-line shall be bisected by an imaginary continuation of the center service-line to a line 4 inches (10 cm.) in length and 2 inches (5 cm.) in width called "the center mark" drawn inside the Court, at right angles to and in contact with such base-lines. All other lines shall be not less than 1 inch (2.5 cm.) nor more than 2 inches (5 cm.) in width, except the base-line which may be not more than 4 inches (10 cm.) in width, and all measurements shall be made to the outside of the lines. All lines shall be of uniform colour. If advertising or any other material is placed at the back of the Court, it may not contain white, or yellow. A light colour may only be used if this does not interfere with the vision of the players.</p>
                                        <p>If advertisement is placed on the chairs of the linemen sitting at the back of the court, they may not contain white or yellow. A light colour may only be used if this does not interfere with the vision of the players.

</p>
                                    </div>
                        </div>
                        
                      
                        
                        <!--bread-crumb-->
                        <blockquote class="styled-blockquote wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="0ms" style="background-image:url(images/resource/breadcrumb-bg.jpg);">
                        	<div class="text">Futures and commodities investments offer investors with more</div>
                        </blockquote>
                        
                        <!--service features-->
                        <div class="service-features">
                        	<div class="row clearfix">
                            	<div class="image-column col-md-5 col-sm-5 col-xs-12">
                                	<figure class="image wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="0ms">
                                    	<img src="images/resource/feature-service.jpg" alt="" />
                                    </figure>
                                </div>
                                <div class="faq-column col-md-7 col-sm-7 col-xs-12">
                                	<h2>Our Key Feature of Service</h2>
                                    
                                    <ul class="accordion-box accordion-box-two wow fadeInRight" data-wow-duration="1500ms" data-wow-delay="0ms">

                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div>Note 1 :</div>
                                            <div class="acc-content current">
                                                <div class="content">
                                                    <p>In Davis Cup, Fed Cup and the Official Championships of the International Tennis Federation, specific requirements with regard to the space behind the baseline and at the sides are included in the respective Regulations for these events.</p>
                                                </div>
                                            </div>
                                        </li>
        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div>Note 2 :</div>
                                            <div class="acc-content">
                                                <div class="content">
                                                    <p>At Club or recreational level, the space behind each baseline should be not less than 18 feet (5.5 m.) and at the sides not less than 10 feet (3.05 m.).</p>
                                                </div>
                                            </div>
                                        </li>
        
                                      
        
                                    </ul>
                                    
                                </div>
                            </div>
                        </div>




                          <!--two-column-->
                        <div class="two-column">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h2>PERMANENT FIXTURES:</h2>
                                    <div class="text">
                                        <p>The permanent fixtures of the court shall include not only the net, posts, singles sticks, cord or metal cable, strap and band, but also, where there are any such, the back and side stops, the stands, fixed movable seats and chairs round the Court, and their occupants, all other fixtures around and above the court, and the Umpire, Net-cord Judge, Foot fault Judge, Linesmen and Ball Boys when in their respective places.</p>
                                        <p>For the purpose of this Rule, the world "Umpire" comprehends the Umpire, the persons entitled to a seat on the Court, and all those persons designed to assist the Umpire in the conduct of a match.

</p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h2>
THE BALL:</h2>
                                    <div class="text">
                                        <p>The ball shall have a uniform outer surface consisting of a fabric cover and shall be white or yellow in colour. If there are any seams they shall be stitches.</p>
                                        <p>The ball shall conform to the requirements specified in Appendix I (Regulations for making tests specified in Rule 3) Section iv for size and be more than two Ounces (56.7 grams) and less than two and one-sixteenth ounces (58.5 grams) in weight.

</p><p>The ball shall have a bound of more than 53 inches (134.62 cm.) and less than 58 inches (147.32 cm.) when dropped 100 inches (254.00 cm.) upon a concrete base.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    
   
    
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>