<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Services</h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index-2.html">Home</a></li>
                    <li class="active">Services</li>
                </ul>
            </div>
            
        </div>
    </section>
    
    <!--we-offer-->
    <section class="we-offer">
    	<div class="auto-container">
        
        	<figure class="right-image wow fadeInRight" data-wow-duration="1500ms" data-wow-delay="0ms">
            	<img class="" src="images/resource/offer-2.jpg" alt="" />
            </figure>
        
        	<div class="row clearfix">
            	<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">
                    <figure class="wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="0ms"><img src="images/resource/we-offer-1.jpg" alt="" /></figure>
                </div>
                    
                <div class="col-lg-5 col-md-7 col-sm-7 col-xs-12">
                    <div class="service-title">Our Service</div>
                    <h2>What We Offer!</h2>
                    <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum been sidue industry's standard</h3>
                    <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum been sidue industry's standard It has survived not only five centuries, but also the leap into electronic</div>
                    
                    <!--call-now-->
                    <div class="call-now" style="background:url(images/resource/call-now-bg.jpg);">
                        <!--call-content-->
                        <div class="call-content clearfix">
                        
                            <!--left-content-->
                            <div class="left-content pull-left">
                                <div class="text">
                                    Have Any Question ?
                                </div>
                                <h4>Call Us Now!!</h4>
                            </div>
                            <!--right-content-->
                            <div class="right-content pull-right">
                                <div class="number">
                                    +(880)1723801729
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    <!--service-style-4-->
    <section class="service-style-four">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="sec-title centered">
                	<h2>Our <span>Services</span></h2>
                </div>
                
                <div class="service-block-four col-md-4 col-sm-6 col-xs-12">
                    <figure class="image">
                        <a href="service-single.html"><img src="images/resource/service-4.jpg" alt=""></a>
                    </figure>
                    <div class="lower-box">
                        <h3><a href="service-single.html">Landscaping Design</a></h3>
                        <div class="text">Vestibulum quam pretium consecte hendrerit mi. Aenean imperdiet sid lacu sit erat amet sie portat malesuada erat bibendum.</div>
                    </div>
                </div>
                
                <div class="service-block-four col-md-4 col-sm-6 col-xs-12">
                    <figure class="image">
                        <a href="service-single.html"><img src="images/resource/service-5.jpg" alt=""></a>
                    </figure>
                    <div class="lower-box">
                        <h3><a href="service-single.html">Tree Planting</a></h3>
                        <div class="text">Vestibulum quam pretium consecte hendrerit mi. Aenean imperdiet sid lacu sit erat amet sie portat malesuada erat bibendum.</div>
                    </div>
                </div>
                
                <div class="service-block-four col-md-4 col-sm-6 col-xs-12">
                    <figure class="image">
                        <a href="service-single.html"><img src="images/resource/service-6.jpg" alt=""></a>
                    </figure>
                    <div class="lower-box">
                        <h3><a href="service-single.html">Rush Removal</a></h3>
                        <div class="text">Vestibulum quam pretium consecte hendrerit mi. Aenean imperdiet sid lacu sit erat amet sie portat malesuada erat bibendum.</div>
                    </div>
                </div>
                
                <div class="service-block-four col-md-4 col-sm-6 col-xs-12">
                    <figure class="image">
                        <a href="service-single.html"><img src="images/resource/service-7.jpg" alt=""></a>
                    </figure>
                    <div class="lower-box">
                        <h3><a href="service-single.html">Garden Care</a></h3>
                        <div class="text">Vestibulum quam pretium consecte hendrerit mi. Aenean imperdiet sid lacu sit erat amet sie portat malesuada erat bibendum.</div>
                    </div>
                </div>
                
                <div class="service-block-four col-md-4 col-sm-6 col-xs-12">
                    <figure class="image">
                        <a href="service-single.html"><img src="images/resource/service-8.jpg" alt=""></a>
                    </figure>
                    <div class="lower-box">
                        <h3><a href="service-single.html">Lawn Maintanance</a></h3>
                        <div class="text">Vestibulum quam pretium consecte hendrerit mi. Aenean imperdiet sid lacu sit erat amet sie portat malesuada erat bibendum.</div>
                    </div>
                </div>
                
               <div class="service-block-four col-md-4 col-sm-6 col-xs-12">
                    <figure class="image">
                        <a href="service-single.html"><img src="images/resource/service-9.jpg" alt=""></a>
                    </figure>
                    <div class="lower-box">
                        <h3><a href="service-single.html">Water Gardening</a></h3>
                        <div class="text">Vestibulum quam pretium consecte hendrerit mi. Aenean imperdiet sid lacu sit erat amet sie portat malesuada erat bibendum.</div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <!--subscribe-style-one-->
    <section class="subscribe-style-one" style="background-image:url(images/background/subscriber-bg.jpg);">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="col-md-7 col-sm-12">
                	<h2>Subscribe Now!!</h2>
                    <div class="text">Lorem ipsum dolor sit amet, mel eu porro sapien tem, ne per idque obliqued </div>
                </div>
                <div class="col-md-5 col-sm-12">
                	<form method="post" action="http://wp1.themexlab.com/html2/gardner_t/contact.html">
                        <div class="form-group">
                            <input type="email" name="email" value="" placeholder="Enter your Email" required>
                            <button type="submit" class="theme-btn"><span class="fa fa-paper-plane-o"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
   

<?php
include "footer.php"
?>
    
  <?php
include "allscript.php";
  ?>
     