<?php
include "allcss.php"
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
  <?php
include "header.php";
  ?>
 
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>About us</h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index-2.html">Home</a></li>
                    <li class="active">About Us</li>
                </ul>
            </div>
            
        </div>
    </section>
    
    <!--about-section-->
    <section class="about-section">
    	<figure class="floated-image wow fadeInRight" data-wow-duration="1500ms" data-wow-delay="0ms">
    		<img src="images/resource/about-us-1.jpg" alt="" />
        </figure>
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="col-md-8 col-sm-12 col-xs-12">
                
                	<div class="col-md-6 col-sm-5 col-xs-12">
                		<figure class="image-box wow slideInLeft" data-wow-duration="1500ms" data-wow-delay="0ms"><img src="images/aboutus/1.jpg" alt="" /></figure>
                    </div>
                    
                    <div class="col-md-6 col-sm-7 col-xs-12">
                        <div class="inner-content">
                           
                        

                      



                            <h2> Coaching</h2>
                            <div class="text">
                                <p>Along with sports, Play Sports Association aims at all round personality development .We provide all sports coaching as well as training and physical conditioning / fitness , metal training and social traits like sportsperson spirit . Coaching covers teaching of all stock production of all games drills with videos. Specific teachical  correction , supervised matal play etc..</p>
                                <p>
All trainees are assessed regularly and the report gives details information about progress of trainees in the area of  sports /physical fitness. Player are promoted to next level after assessment.</p>
                            </div>

                         
                            <br>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12 col-xs-12">
                	<div class="about-block">
                    	<figure class="image wow fadeIn" data-wow-duration="1500ms" data-wow-delay="0ms">
                        	<a href="#"><img src="images/aboutus/about.jpg" alt="" /></a>
                              
                        </figure>
                        <div class="lower-box"><a href="#">
                        	<h3>Our Association</h3><img src="images/1.jpg" alt="" /></a>
                            <p> Promote the game from School Level to Elite Level Create a robust Tournament Structure for Junior to Senior Level Players Conduct State, National Level Tournaments. 


                           </p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    
    
    <!--Two COlumn Fluid-->
    <section class="two-col-fluid">
    	<div class="outer-container clearfix">
        	<!--Image Column-->
            <div class="image-column" style="background-image:url(images/background/image-3.jpg);"><figure class="image-box"><img src="images/background/image-3.jpg" alt=""></figure></div>
            <!--Content Column-->
            <div class="content-column content-column-two">
            	<div class="inner-box clearfix">
                	<div class="sec-title-two">



                    	<h2>Programmes <span>of coaching</span></h2>
                    </div>
                    
                    <div class="col-xs-12">
                    	<div class="row">
                    	<h3><span>1.</span>Beginner Regular Group.</h3>
                       <h3><span>2.</span>Intermediate Group Coaching</h3>
                       <h3><span>3.</span>Advance Group Coaching</h3>
                         <div class="text">
Aims of this scheme is to introduce all games this all age groups is between 4 to 18 years.
Training duration is 1 hrs and physical training 1 hrs.</div>
                        </div>
                    </div>
                    
                  
                    
                </div>
            </div>
        </div>
    </section>
    
 
       


 <section class="service-style-two">
            <div class="auto-container">
                <div class="sec-title centered">
                    <h2>6 REASONS WHY<span>  SPORTS AND PHYSICAL EDUCATION IS IMPORTANT FOR YOUR CHILD</span></h2>
                </div>

                <div class="row">
                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-life-bouy"></span>
                            </div>
                            <h2>Loco-motor Skills</h2>
                            <div class="text">A loco-motor skill helps the body to do physical movements effectively. Examples of physical movements include running, galloping, hopping, sliding, skipping, etc. More often these skills are used, greater is the child’s ability to fine tune the movement of each skill. For example, improving running skills will enable the child to participate in relay races and sprints. It is an important part of physical education training.</div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-linode"></span>
                            </div>
                            <h2>Object Control Skills</h2>
                            <div class="text">Object control skills require controlling implements and objects such as balls, hoops, bats and ribbons by hand, by foot or with any other part of the body. Object control requires lot of practice and concentration. This improves the movement and strength of the body. This helps in improving a physical as well as mental health of the child. Object control is an essential part of various sports coaching.</div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-mars-double"></span>
                            </div>
                            <h2>Physical Fitness</h2>
                            <div class="text">Physical fitness is a state of health and well-being and the ability to perform various sports and physical activities. It is achieved through proper nutrition, physical exercise and sufficient rest. It is taken care of during physical education and sports coaching. Nutrition should also be taken care at home by parents.</div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-users"></span>
                            </div>
                            <h2>Social</h2>
                            <div class="text">Social skills include self discipline, cooperation, sportsmanship, handling equipments, etc. These skills have a meaningful impact on a kid’s behavior. These are taught to kids during the physical education class and during sports coaching


</div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                              <span class="fa fa-angellist"></span>    
                            </div>
                            <h2>Body Management</h2>
                            <div class="text"> These skills mean controlling the body in certain situation. Depending on the exercise or movement body have adjust itself in different ways. These skills require agility, flexibility, coordination, movement. Sometimes non-motor skills play an important part too, wherein a child has to bend, twist, push, pull or stretch. Space awareness also is a critical part of various sports, which is taught in Body Management. </div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-viacoin"></span>
                            </div>
                            <h2>Game and Sport Skill</h2>
                            <div class="text">Different sports require different sports skills. Kids should be aware about these skills and rules, regulations and safety guidelines. Team sports inculcate team spirit and belongingness in the kids. They learn that participation is more important than winning or losing.</div>
                        </div>
                    </div>

                </div>

            </div>
        </section>



  <!--subscribe-style-one-->
    <section class="subscribe-style-one" style="background-image:url(images/background/subscriber-bg.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-sm-12">
                   
                    <div style="font-size:20px" class="text">There are various things, we have to keep in mind for overall growth of child. These are some of the building blocks for the long term health of the kids. Everyone have to work hard towards achieving any goal for life.The play sports association have a comprehensive blog containing very informative articles for physical education, sports and fitness.</div>
                </div>
              
            </div>
        </div>
    </section>

 <!--team-section-->
    <section class="team-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our <span>Team</span></h2>
            </div>
        
            <div class="row clearfix">
                
              
   <?php

                                               include "db.php";

$result = mysqli_query($con,"SELECT * FROM team limit 4");
while($row = mysqli_fetch_array($result))
{

echo '  
                <div class="col-md-3 col-sm-6 col-xs-12 column team-member-one">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                        <figure class="image">
                            <a href="#"><img src="admin/photos/team/'.$row['img'].'" alt="" /></a>
                            
                           
                          
                            
                        </figure>
                        <div class="lower-box">
                            <h3>'.$row['name'].'</h3>
                            <div class="text">'.$row['post'].'</div>
                        </div>
                    </div>
                </div>




';
}
?>
             
             
                
            </div>
        </div>
    </section>
    



   
    
    <!--subscribe-style-one-->
    <section class="subscribe-style-one" style="background-image:url(images/background/subscriber-bg.jpg);">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="col-md-12 col-sm-12">
                	<center><h2>"Better Coach" "Better Player" "Better Game" !!</h2></center>
                  
                </div>
               
            </div>
        </div>
    </section>
    
 

<?php
include "footer.php"
?>
    
  <?php
include "allscript.php";
  ?>
     