<?php
   session_start();
   if(!isset($_SESSION["username"])){
   header("Location: login.php");
   exit(); }
   ?>


<?php 
include "allcss.php";
?>

<?php
include "header.php";
?>

        <div class="content-page">
            <div class="content">
                <div class="">
                    <div class="page-header-title">
                        <h4 class="page-title">Team</h4></div>
                </div>
                <div class="page-content-wrapper ">
                    <div class="container">
                       
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel-body">

                                  
  

<?php

    error_reporting( ~E_NOTICE );
    
    require_once 'dbconfig.php';
    
    if(isset($_GET['edit_id']) && !empty($_GET['edit_id']))
    {
        $id = $_GET['edit_id'];
        $stmt_edit = $DB_con->prepare('SELECT * FROM admission WHERE id =:id');
        $stmt_edit->execute(array(':id'=>$id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);
    }
    else
    {
        header("Location: admission.php");
    }
    
    

       if(isset($_POST['btn_save_updates']))
    {
        $name = $_POST['name'];
          $fname = $_POST['fname'];
            $lastname = $_POST['lastname'];
              $dob = $_POST['dob'];
                $pob = $_POST['pob'];
                  $schoolname = $_POST['schoolname'];
                    $email = $_POST['email'];
  $phone = $_POST['phone']; 
   $lastname = $_POST['lastname']; 
    $profession = $_POST['profession'];
      $address = $_POST['address'];                
       
        if(!isset($errMSG))
        {
$stmt = $DB_con->prepare('UPDATE admission SET name=:name, fname=:fname,lastname=:lastname,dob=:dob,pob=:pob,schoolname=:schoolname,
    email=:email,phone=:phone,lastname=:lastname,profession=:profession,address=:address
    WHERE id=:id');
          
            $stmt->bindParam(':name',$name);    
             $stmt->bindParam(':fname',$fname); 
                $stmt->bindParam(':lastname',$lastname); 
                   $stmt->bindParam(':dob',$dob); 
                      $stmt->bindParam(':pob',$pob); 
                         $stmt->bindParam(':schoolname',$schoolname); 
                            $stmt->bindParam(':email',$email); 
                               $stmt->bindParam(':phone',$phone); 
   $stmt->bindParam(':lastname',$lastname); 
      $stmt->bindParam(':profession',$profession); 
         $stmt->bindParam(':address',$address); 

            $stmt->bindParam(':id',$id);
                
            if($stmt->execute()){
                ?>
                <script>
                alert('Successfully Updated ...');
                window.location.href='admission.php';
                </script>
                <?php
            }
            else{
                $errMSG = "Sorry Data Could Not Updated !";
            }
        
        }
        
                        
    }
    
?>


                                      
                                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">

                                          <div class="form-group">
                                                <label class="col-md-2 control-label">First Name</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $name; ?>" name="name"  class="form-control" placeholder="post">
                                                </div>
                                            </div>

                                              <div class="form-group">
                                                <label class="col-md-2 control-label">Middle Name</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $fname; ?>" name="fname"  class="form-control" placeholder="name">
                                                </div>
                                            </div>

   <div class="form-group">
                                                <label class="col-md-2 control-label">Last Name</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $lastname; ?>" name="lastname"  class="form-control" placeholder="name">
                                                </div>
                                            </div>




  <div class="form-group">
                                                <label class="col-md-2 control-label">DOB</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $dob; ?>" name="dob"  class="form-control" placeholder="post">
                                                </div>
                                            </div>

                                              <div class="form-group">
                                                <label class="col-md-2 control-label">POB</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $pob; ?>" name="pob"  class="form-control" placeholder="name">
                                                </div>
                                            </div>

   <div class="form-group">
                                                <label class="col-md-2 control-label">School Name</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $schoolname; ?>" name="schoolname"  class="form-control" placeholder="name">
                                                </div>
                                            </div>

  <div class="form-group">
                                                <label class="col-md-2 control-label">Email Id</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $email; ?>" name="email"  class="form-control" placeholder="post">
                                                </div>
                                            </div>

                                              <div class="form-group">
                                                <label class="col-md-2 control-label">Mobile</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $phone; ?>" name="phone"  class="form-control" placeholder="name">
                                                </div>
                                            </div>

   <div class="form-group">
                                                <label class="col-md-2 control-label">Profession</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $profession; ?>" name="profession"  class="form-control" placeholder="name">
                                                </div>
                                            </div>

<div class="form-group">
                                                <label class="col-md-2 control-label">address</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $address; ?>" name="address"  class="form-control" placeholder="name">
                                                </div>
                                            </div>
                                          

                                            <div class="form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                  <input class="btn btn-primary pull-right" type="submit" name="btn_save_updates" value="Update Admission" />



                                                    
                                                </div>
                                            </div>

                                          
                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>



                      
                    </div>
                </div>
            </div>
             <?php
include "allscript.php";
           ?>