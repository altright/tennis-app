<?php
   session_start();
   if(!isset($_SESSION["username"])){
   header("Location: login.php");
   exit(); }
   ?>


<?php 
include "allcss.php";
?>

<?php
include "header.php";
?>

        <div class="content-page">
            <div class="content">
                <div class="">
                    <div class="page-header-title">
                        <h4 class="page-title">Team</h4></div>
                </div>
                <div class="page-content-wrapper ">
                    <div class="container">
                       
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel-body">

                                  
  

<?php

    error_reporting( ~E_NOTICE );
    
    require_once 'dbconfig.php';
    
    if(isset($_GET['edit_id']) && !empty($_GET['edit_id']))
    {
        $id = $_GET['edit_id'];
        $stmt_edit = $DB_con->prepare('SELECT name,post, img FROM team WHERE id =:id');
        $stmt_edit->execute(array(':id'=>$id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);
    }
    else
    {
        header("Location: team.php");
    }
    
    
    
    if(isset($_POST['btn_save_updates']))
    {
        $name = $_POST['name'];
         $post = $_POST['post'];

        $imgFile = $_FILES['user_image']['name'];
        $tmp_dir = $_FILES['user_image']['tmp_name'];
        $imgSize = $_FILES['user_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'photos/team/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $img = rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['img']);
                    move_uploaded_file($tmp_dir,$upload_dir.$img);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $img = $edit_row['img']; // old image from database
        }   
                        
        
        // if no error occured, continue ....
        if(!isset($errMSG))
        {
$stmt = $DB_con->prepare('UPDATE team SET name=:name,post=:post, img=:img
    WHERE id=:id');
            $stmt->bindParam(':name',$name);   
            $stmt->bindParam(':post',$post); 
            $stmt->bindParam(':img',$img);
             
            $stmt->bindParam(':id',$id);
                
            if($stmt->execute()){
                ?>
                <script>
                alert('Successfully Updated ...');
                window.location.href='team.php';
                </script>
                <?php
            }
            else{
                $errMSG = "Sorry Data Could Not Updated !";
            }
        
        }
        
                        
    }
    
?>


                                      
                                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">


                                            



                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Choose Image</label>
                                                <div class="col-md-10">
                                                     <img src="photos/team/<?php echo $img; ?>" height="130" width="150" />

            
            
    <input type="file" class="fileinput btn-primary"  name="user_image" accept="image/*" />



                                                    
                                                </div>
                                            </div>
                                           
                                            



                                              <div class="form-group">
                                                <label class="col-md-2 control-label">Post</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $post; ?>" name="post"  class="form-control" placeholder="post">
                                                </div>
                                            </div>

                                              <div class="form-group">
                                                <label class="col-md-2 control-label">name</label>
                                                <div class="col-md-10">
                                                    <input type="text" value="<?php echo $name; ?>" name="name"  class="form-control" placeholder="name">
                                                </div>
                                            </div>


                                          

                                            <div class="form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                  <input class="btn btn-primary pull-right" type="submit" name="btn_save_updates" value="Update photo" />



                                                    
                                                </div>
                                            </div>

                                          
                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>



                      
                    </div>
                </div>
            </div>
             <?php
include "allscript.php";
           ?>