<?php
   session_start();
   if(!isset($_SESSION["username"])){
   header("Location: login.php");
   exit(); }
   ?>


   <?php 
include "allcss.php";
?>

<?php
include "header.php";
?>
        <div class="content-page">
            <div class="content">
                <div class="">
                    <div class="page-header-title">
                        <h4 class="page-title">Dashboard</h4></div>
                </div>
                <div class="page-content-wrapper ">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="panel text-center">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-muted font-light">Total Subscription</h4></div>
                                    <div class="panel-body p-t-10">
                                        <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-down text-danger m-r-10"></i><b>8952</b></h2>
                                        <p class="text-muted m-b-0 m-t-20"><b>48%</b> From Last 24 Hours</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="panel text-center">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-muted font-light">Order Status</h4></div>
                                    <div class="panel-body p-t-10">
                                        <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b>6521</b></h2>
                                        <p class="text-muted m-b-0 m-t-20"><b>42%</b> Orders in Last 10 months</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="panel text-center">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-muted font-light">Unique Visitors</h4></div>
                                    <div class="panel-body p-t-10">
                                        <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b>452</b></h2>
                                        <p class="text-muted m-b-0 m-t-20"><b>22%</b> From Last 24 Hours</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="panel text-center">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-muted font-light">Monthly Earnings</h4></div>
                                    <div class="panel-body p-t-10">
                                        <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-down text-danger m-r-10"></i><b>5621</b></h2>
                                        <p class="text-muted m-b-0 m-t-20"><b>35%</b> From Last 1 Month</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
           <?php
include "allscript.php";
           ?>