


<body class="fixed-left">
    <div id="wrapper">
        <div class="topbar">
            <div class="topbar-left">
                <div class="text-center">
                    <a href="index.php" class="logo"><img src="assets/images/logo.png"></a>
                    <a href="index.php" class="logo-sm"><img src="assets/images/logo_sm.png"></a>
                </div>
            </div>
            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="">
                        <div class="pull-left">
                            <button type="button" class="button-menu-mobile open-left waves-effect waves-light"> <i class="ion-navicon"></i> </button> <span class="clearfix"></span></div>
                       
                        <ul class="nav navbar-nav navbar-right pull-right">
                          
                            <li class="hidden-xs"> <a href="#" id="btn-fullscreen" class="waves-effect waves-light notification-icon-box"><i class="mdi mdi-fullscreen"></i></a></li>
                            
    <li ><a  style="padding:20px;" href="logout.php"><b style=" color:#04a2b3"> Logout</b></a></li>      


                          
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">
                <div id="sidebar-menu">
                    <ul>
                        <li style=" text-align:  center;" class="menu-title">      
                        <?php echo $_SESSION['username']; ?></li>
                       <li> <a href="index.php" class="waves-effect"><i class="mdi mdi-home"></i><span> Dashboard <span class="badge badge-primary pull-right">1</span></span></a></li> 

                     

                          <li> <a href="news.php" class="waves-effect"><i class="mdi mdi-newspaper"></i><span> News</a></li>

  <li> <a href="photo.php" class="waves-effect"><i class="mdi mdi-image-area"></i><span>Photo Gallery</a></li>

    <li> <a href="video.php" class="waves-effect"><i class="mdi mdi-file-video"></i><span>Video Gallery</a></li>

      <li> <a href="slider.php" class="waves-effect"><i class="mdi mdi-image-album"></i><span> Slider</a></li>

      <li> <a href="admission.php" class="waves-effect"><i class="mdi mdi-account-box"></i><span>Admission Data</a></li>

        <li> <a href="registration.php" class="waves-effect"><i class="mdi mdi-account-box"></i><span> Registration</a></li>


 <li> <a href="contactus.php" class="waves-effect"><i class="mdi mdi-account-box"></i><span> Contact </a></li>

         <li> <a href="games.php" class="waves-effect"><i class="mdi mdi-chart-pie"></i><span> Sports Category</a></li>


         <li> <a href="blogs.php" class="waves-effect"><i class="mdi mdi-blogger"></i><span> Blogs</a></li>

      <li> <a href="promoters.php" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Promoters</a></li>
           

           <li> <a href="partners.php" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Partners</a></li>
        
              <li> <a href="team.php" class="waves-effect"><i class="mdi mdi-account-key"></i><span> Team</a></li>

                       <!--  <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-album"></i> <span> UI Elements </span> <span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="ui-components.php">Components</a></li>
                                <li><a href="ui-buttons.php">Buttons</a></li>
                                <li><a href="ui-panels.php">Panels</a></li>
                                <li><a href="ui-tabs-accordions.php">Tabs &amp; Accordions</a></li>
                                <li><a href="ui-modals.php">Modals</a></li>
                                <li><a href="ui-progressbars.php">Progress Bars</a></li>
                                <li><a href="ui-alerts.php">Alerts</a></li>
                                <li><a href="ui-sweet-alert.php">Sweet-Alert</a></li>
                                <li><a href="ui-grid.php">Grid</a></li>
                            </ul>
                        </li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span> Forms </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="form-elements.php">General Elements</a></li>
                                <li><a href="form-validation.php">Form Validation</a></li>
                                <li><a href="form-advanced.php">Advanced Form</a></li>
                                <li><a href="form-wysiwyg.php">WYSIWYG Editor</a></li>
                                <li><a href="form-uploads.php">Multiple File Upload</a></li>
                            </ul>
                        </li>
                        <li> <a href="typography.php" class="waves-effect"><i class="mdi mdi-diamond"></i><span> Typography <span class="badge badge-primary pull-right">4</span></span></a></li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-table"></i><span> Tables </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="tables-basic.php">Basic Tables</a></li>
                                <li><a href="tables-datatable.php">Data Table</a></li>
                                <li><a href="tables-responsive.php">Responsive Table</a></li>
                                <li><a href="tables-editable.php">Editable Table</a></li>
                            </ul>
                        </li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-pie"></i><span> Charts </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="charts-morris.php">Morris Chart</a></li>
                                <li><a href="charts-chartjs.php">Chartjs</a></li>
                                <li><a href="charts-flot.php">Flot Chart</a></li>
                                <li><a href="charts-other.php">Other Chart</a></li>
                            </ul>
                        </li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-opacity"></i> <span> Icons </span> <span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="icons-material.php">Material Design</a></li>
                                <li><a href="icons-ion.php">Ion Icons</a></li>
                                <li><a href="icons-fontawesome.php">Font awesome</a></li>
                                <li><a href="icons-themify.php">Themify Icons</a></li>
                            </ul>
                        </li>
                        <li class="menu-title">Features</li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map"></i><span> Maps </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="maps-google.php"> Google Map</a></li>
                                <li><a href="maps-vector.php"> Vector Map</a></li>
                            </ul>
                        </li>
                        <li> <a href="calendar.php" class="waves-effect"><i class="mdi mdi-calendar"></i><span> Calendar <span class="badge badge-primary pull-right">NEW</span></span></a></li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-assistant"></i><span> Layouts </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="layouts-collapse.php">Menu Collapse</a></li>
                                <li><a href="layouts-smallmenu.php">Menu Small</a></li>
                                <li><a href="layouts-menu2.php">Menu Style 2</a></li>
                            </ul>
                        </li>
                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-pages"></i><span> Pages </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="pages-login.php">Login</a></li>
                                <li><a href="pages-register.php">Register</a></li>
                                <li><a href="pages-recoverpw.php">Recover Password</a></li>
                                <li><a href="pages-lock-screen.php">Lock Screen</a></li>
                                <li><a href="pages-blank.php">Blank Page</a></li>
                                <li><a href="pages-404.php">Error 404</a></li>
                                <li><a href="pages-500.php">Error 500</a></li>
                                <li><a href="pages-timeline.php">Timeline</a></li>
                                <li><a href="pages-invoice.php">Invoice</a></li>
                                <li><a href="pages-directory.php">Directory</a></li>
                            </ul>
                        </li>


                        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-share-variant"></i><span>Multi Menu </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                            <ul>
                                <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><span>Menu Item 1.1</span> <span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                                    <ul style="">
                                        <li><a href="javascript:void(0);"><span>Menu Item 2.1</span></a></li>
                                        <li><a href="javascript:void(0);"><span>Menu Item 2.2</span></a></li>
                                    </ul>
                                </li>
                                <li> <a href="javascript:void(0);"><span>Menu Item 1.2</span></a></li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>