<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Swimming </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Sports Category </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li  ><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li ><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li ><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li ><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li ><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>
                                     <li><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li  class="current"> <a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                            	
                        <!--Default-section-one-->
                        <div class="service-images">
                        	<div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <figure class="image">
                                        <a href="images/swim.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/swim.jpg" alt="" /></a>
                                    </figure>
                                    
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                    <figure class="image">
                                        <a href="images/swim2.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/swim2.jpg"  alt="" /></a>
                                    </figure>
                                    <figure class="image">
                                        <a href="images/swim1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/swim1.jpg" alt="" /></a>
                                    </figure>
                                </div>
                             </div>
                        </div>
                        
                        <!--text-content-->
                        <div class="text-content">
                        	<h2>Swimming </h2>
                            <div class="bold-text">Swimming is the self-propulsion of a person through fresh or salt water, usually for recreation, sport, exercise, or survival. Locomotion is achieved through coordinated movement of the limbs, the body, or both. Humans can hold their breath underwater and undertake rudimentary locomotive swimming within weeks of birth, as an evolutionary response</div>
                            <div class="text">Swimming relies on the natural/non natural buoyancy of the human body. On average, the body has a relative density of 0.98 compared to water, which causes the body to float. However, buoyancy varies on the basis of both body composition and the salinity of the water. Higher levels of body fat and saltier water both lower the relative density of the body and increase its buoyancy.

Since the human body is only slightly less dense than water, water supports the weight of the body during swimming. As a result, swimming is “low-impact” compared to land activities such as running. The density and viscosity of water also create resistance for objects moving through the water. Swimming strokes use this resistance to create propulsion, but this same resistance also generates drag on the body.

</div>

                        </div>
                        
                      
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    
   
    
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>