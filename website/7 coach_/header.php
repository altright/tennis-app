   <!-- Main Header-->
    <header class="main-header">
        <div class="header-style-2">
        
            <!-- Header Top -->
            <div class="header-top">
                <div class="auto-container clearfix">
                    <!--Top Left--> <div class="top-left pull-left">
                        <ul class="links-nav clearfix">
                         <li style="
    background-color: #de0000;padding:10px;
     color: white; 
"> <a  style=" color: white; " href="admission.php">Admission Form</a> </li>
                            
                        </ul>
                    </div>
                  
                    
                    <!--Top Right-->
                    <div class="top-right pull-right">
                        <ul class="links-nav clearfix">
                            <li><a target="_blank" href="https://www.facebook.com/Krishna-Tennis-Academy-Mumbai-454289058250294/?hc_ref=ARSNtqQv-o5Ey5OWLWLgx9wk005qP5vEuX5IQmFvxE0ZEm9Guvd3d1rU76H_coSGbDw&fref=nf"><span class="fa fa-facebook-f"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Header Top End -->
            
            <!--Header-Upper-->
            <div class="header-upper">
                <div class="auto-container">
                    <div class="clearfix">
                        
                        <div class="pull-left logo-outer">
                            <div class="logo"><a href="index.php"><img src="images/logo.png" width="320px"  alt="Gardner" title="Gardner"></a></div>
                        </div>
                        
                        <div class="pull-right upper-right clearfix">
                            
                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="icon-box"><span class="flaticon-phone-call"></span></div>
                                <ul>


                                <li><strong>+91 88 9841 5767 <br>+91 70 4552 5557</strong></li>
                                <li>krishnatennis9@gmail.com</li>
                            </ul>
                            </div>
                            
                           
                            
                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="icon-box"><span class="flaticon-placeholder-1"></span></div>
                               <ul>


                              <li><strong>Dadoji Kondev Ground Opp,</strong></li>
                                <li>Vishnu Shivam Mall, Behind Victory Court,</li>
                                  <li> Thakur Village Kandivali [E] Mumbai - 4001 01.</li>
                            </ul>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <!--Header-Lower-->
            <div class="header-lower">
                <!--Background Layer-->
                <div class="bg-layer"></div>
                
                
                <div class="auto-container">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                   <li><a href="index.php">Home</a>
                                   
                                </li>
                               <li ><a href="about-us.php">About Us</a>
                                    <!--<ul>
                                       
                                        <li><a href="objective.php">Our Objective</a></li>
                                         <li><a href="vision.php">Vision & Mission</a></li>
                                        <li><a href="bm.php">Board Members</a></li>
                                         <li><a href="coach.php">Coach Names</a></li>
                                    </ul> -->
                                </li>
                                <li class="dropdown"><a href="category.php">Sports Category</a>
                                     <ul>
                                        <li><a href="tennis.php">Lawn Tennis</a></li>
                                        <li><a href="football.php">Football</a></li>
                                         <li><a href="kabbadi.php">Kabbadi</a></li>
                                        <li><a href="cricket.php">Cricket</a></li>
                                         <li><a href="athelete.php">Athelete</a></li>
                                        <li><a href="basketball.php">Basket Ball</a></li>

                                            <li><a href="skating.php">Skating</a></li>
                                         <li><a href="swimming.php">Swimming</a></li>
                                        <li><a href="yoga.php">Yoga</a></li>

                                    </ul>
                                </li>
                               <li  class="dropdown"><a href="gallery.php">Gallery</a>
                                     <ul>
                                        <li><a href="photo.php">Photo Gallery</a></li>
                                        <li><a href="video.php">Video Gallery</a></li>
                                    </ul>
                                </li>
                               

                                 <li><a href="news.php">News & Events</a>
                                   
                                </li>
   <li><a href="blogs.php">Blogs</a>
                                   
                                </li>
                                  <li><a href="contact.php">Contact us</a>
                                   
                                </li>
                                 
                              
                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                    
                        <div class="btn-outer-two"><a href="booking.php" class="theme-btn quote-btn"><span class="border"></span>Booking</a></div>
                        
                    </div>
                </div>
            </div>
            
            <?php
include "stickyheader.php";
        ?>
      
            
        </div>
    </header>
    <!--End Main Header -->