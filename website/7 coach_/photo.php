<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Photo Gallery</h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Photo Gallery</li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Gallery Section-->
    <section class="gallery-section-two">
    	<div class="auto-container">

            <div class="mixitup-gallery">
        		<!--Filter-
                <div class="filters text-center">
                    <ul class="filter-tabs filter-btns clearfix">
                        <li class="active filter" data-role="button" data-filter="all"><span class="txt">ALL</span></li>
                        <li class="filter" data-role="button" data-filter=".desing"><span class="txt">Lawn Tennis</span></li>
                        <li class="filter" data-role="button" data-filter=".rubbish"><span class="txt">FootBall</span></li>
                        <li class="filter" data-role="button" data-filter=".lawn"><span class="txt">Kabbadi</span></li>
                        <li class="filter" data-role="button" data-filter=".garden"><span class="txt">Cricket</span></li>
                        <li class="filter" data-role="button" data-filter=".water"><span class="txt">Athelete</span></li>
                         <li class="filter" data-role="button" data-filter=".water"><span class="txt">Basket Ball</span></li>
                    </ul>
                </div>

                <!--Filter List-->
                <div class="filter-list row clearfix">








   <?php

                                               include "db.php";

$result = mysqli_query($con,"SELECT * FROM photo");
while($row = mysqli_fetch_array($result))
{

echo '

                    <div class="gallery-item col-md-4 col-sm-6 col-xs-12 mix mix_all all lawn garden water">
                        <div class="inner-box">
                            <div class="image-box"><img src="admin/photos/photo/'.$row['img'].'" alt=""></div>
                           
                            <div class="caption">
                              '.$row['title'].'
                            </div>
                            <div class="overlay-box">
                                <div class="inner">
                                    <div class="content">
                                        <h2><a href="#">'.$row['title'].'</a></h2>
                                        
                                        <a href="admin/photos/photo/'.$row['img'].'" class="plus lightbox-image">
                                            <span class="flaticon-add-2"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                       

';
}
?>
            

                </div>

            </div>
        </div>
    </section>
    
    <!--subscribe-style-one-->
    <section class="subscribe-style-one" style="background-image:url(images/background/subscriber-bg.jpg);">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="col-md-12 col-sm-12">
                	<center><h2>"Better Coach" "Better Player" "Better Game" !!</h2></center>
                   
                </div>
               
            </div>
        </div>
    </section>
    
   
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>