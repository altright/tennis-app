<?php
include "allcss.php";
?>

<script src="jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="script/getData.js"></script>

<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
   
        
    <!--Contact Section-->
    <section class="contact-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column  col-xs-12">
                    <h2>Admission Form</h2>
                        <div class="contact-form default-form">
 <?php
include"db.php";
// INSERT
if(isset($_POST['save']))
{

$name = $con->real_escape_string($_POST['name']);
$fname = $con->real_escape_string($_POST['fname']);
$lastname = $con->real_escape_string($_POST['lastname']);
$dob = $con->real_escape_string($_POST['dob']);
$pob = $con->real_escape_string($_POST['pob']);
$schoolname = $con->real_escape_string($_POST['schoolname']);
$email = $con->real_escape_string($_POST['email']);
$phone = $con->real_escape_string($_POST['phone']);
$profession = $con->real_escape_string($_POST['profession']);
$address = $con->real_escape_string($_POST['address']);

$result = mysqli_query($con,"INSERT INTO admission (name,fname,lastname,profession,dob, pob,email,schoolname,address,phone) VALUES('$name','$fname','$lastname','$profession','$dob','$pob','$email','$schoolname','$address','$phone')");
    
                ?>
                <script>
                alert('Successfully Registered ...');
                window.location.href='admission.php';
                </script>
                <?php
          
}
?>

    <form action="" method="post" accept-charset="utf-8"  enctype="multipart/form-data">
                                <div class="row">
                                   


                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">

                                            <input type="text" name="name" value="" placeholder="First Name" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="fname" value="" placeholder="Middle Name" required="">
                                        </div>
                                    </div>

                                   <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="lastname" value="" placeholder="Last Name" required="">
                                        </div>
                                    </div>







                                   

                                     <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                    
                                            <input  type="text" name="dob" value="" placeholder="Date of Birth" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="pob" value="" placeholder="Place of Birth" >
                                        </div>
                                    </div>
                                      <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="email" name="email" value="" placeholder="Email Id" required="">
                                        </div>
                                    </div>
                                   

                                <div class="col-md-8 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="schoolname" value="" placeholder="School Name" >
                                        </div>
                                    </div>

                                
                                    
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <textarea name="address" placeholder="Address" required=""></textarea>
                                        </div>
                                    </div>
                                    


                                     <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="phone" value="" placeholder="Mobile Number" required="">
                                        </div>
                                    </div>

                                     <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="profession" value="" placeholder="Profession of Guardian" >
                                        </div>
                                    </div>
                                    

                                    
 <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                         <input class="theme-btn btn-style-three" type="submit" name="save" value="Submit" />
</div></div>

                                
                                   
                            </div>
                    

                    </div>
                </div>
</form>
               
                </div>

            </div>
        </div>
    </section>

    
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>