<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Kabbadi </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Sports Category </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li  ><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li class="current"><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>
                                     <li><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li><a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                            	
                        <!--Default-section-one-->
                        <div class="service-images">
                        	<div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/kabadi.jpg" alt="" /></a>
                                    </figure>
                                    
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-2.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/kabadi1.jpg" alt="" /></a>
                                    </figure>
                                    <figure class="image">
                                        <a href="images/resource/service-detail-3.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/kabadi2.jpg" alt="" /></a>
                                    </figure>
                                </div>
                             </div>
                        </div>
                        
                        <!--text-content-->
                        <div class="text-content">
                        	<h2>kabaddi </h2>
                            <div class="bold-text">Kabaddi is a contact team sport.The game is said to have had its inception in the southern state of Tamil Nadu, India. The word ‘kabaddi’ owes its origin to the Tamil word, ‘kai-pidi’, which means holding hands.</div>
                            <div class="text">Kabaddi is played between two teams of seven players; the object of the game is for a single player on offence, referred to as a "raider", to run into the opposing team's half of a court, tag out as many of their defenders as possible, and return to their own half of the court, all without being tackled by the defenders. Points are scored for each player tagged by the raider, while the opposing team earns a point for stopping the raider. Players are taken out of the game if they are tagged or tackled, but can be "revived" for each point scored by their team from a tag or tackle.</div>
<br>
                             <h2>Our Challanging Part</h2>
                                    <div class="text">
                                        <p>A point is scored for each defender tagged. If the raider steps beyond the bonus line marked in enemy territory, they earn an additional point. If the raider is successfully stopped, the opposing team earns a point instead. All players tagged are taken out of the game, but one is "revived" for each point a team scores from a subsequent tag or tackle (bonus points do not revive players). Players who step out of bounds are also out. A raid where no points are scored by the raider is referred to as an "empty raid". By contrast, a play where the raider scores three or more points is referred to as a "super raid". If a team gets all seven players on the opposing team out at once, an "All Out" is scored for two bonus points, and they are automatically revived.

</p>
                                    </div>
                        </div>
                        
                      
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    
   
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>