<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Skating </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Sports Category </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li  ><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li ><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li ><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li ><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li ><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>
                                     <li class="current"><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li><a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                            	
                        <!--Default-section-one-->
                        <div class="service-images">
                        	<div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <figure class="image">
                                        <a href="images/skating.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/skating.jpg" alt="" /></a>
                                    </figure>
                                    
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                    <figure class="image">
                                        <a href="images/skating1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/skating2.jpg"  alt="" /></a>
                                    </figure>
                                    <figure class="image">
                                        <a href="images/skating3.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/skating3.png" alt="" /></a>
                                    </figure>
                                </div>
                             </div>
                        </div>
                        
                        <!--text-content-->
                        <div class="text-content">
                        	<h2>Skating </h2>
                            <div class="bold-text">Roller skating is the traveling on surfaces with roller skates.</div>
                            <div class="text"> It is a form of recreational activity as well as a sport, and can also be a form of transportation. Skates generally come in three basic varieties: quad roller skates, inline skates or blades and tri-skates, though some have experimented with a single-wheeled "quintessence skate" or other variations on the basic skate design. In America, this hobby was most popular, first between 1935 and the early 1960s and then in the 1970s, when polyurethane wheels were created and disco music oriented roller rinks were the rage and then again in the 1990s when in-line outdoor roller skating, thanks to the improvement made to inline roller skates in 1981 by Scott Olson, took hold. <br>

                            Speed skating originally started on traditional roller skates. The speed skating season began in fall and continued through spring leading up to a state tournament. Placing in the top three places at a state tournament would qualify skaters for a regional tournament. The top three places at regional tournaments then went on to compete at a national tournament. Skaters could qualify as individuals or as part of a two-person or four-person (relay) team. Qualification at regional events could warrant an invite to the Olympic Training Center in Colorado Springs, CO for a one-week training session on their outdoor velodrome. Inline speed skating is a competitive non-contact sport played on inline skates. Variants include indoor, track and road racing, with many different grades of skaters, so the whole family can compete.

</div>

                        </div>
                        
                      
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    

    
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>