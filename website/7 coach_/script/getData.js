$(document).ready(function(){  
	// code to get all records from table via select box
	$("#games").change(function() {    
		var id = $(this).find(":selected").val();
		var dataString = 'gameid='+ id;    
		$.ajax({
			url: 'getgame.php',
			dataType: "json",
			data: dataString,  
			cache: false,
			success: function(gamedata) {
			   if(gamedata) {
					$("#heading").show();		  
					$("#no_records").hide();					
					$("#game").text(gamedata.game);
					$("#amount").text(gamedata.amount);
					
					$("#records").show();		 
				} else {
					$("#heading").hide();
					$("#records").hide();
					$("#no_records").show();
				}   	
			} 
		});
 	}) 
});
