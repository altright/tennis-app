<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Football </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Sports Category </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li  class="current"><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>
                                     <li><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li><a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                            	
                        <!--Default-section-one-->
                        <div class="service-images">
                        	<div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/foot.jpg" alt="" /></a>
                                    </figure>
                                    
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-2.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/foot1.jpg" alt="" /></a>
                                    </figure>
                                    <figure class="image">
                                        <a href="images/resource/service-detail-3.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/foot2.jpg" alt="" /></a>
                                    </figure>
                                </div>
                             </div>
                        </div>
                        
                        <!--text-content-->
                        <div class="text-content">
                        	<h2>Football </h2>
                            <div class="bold-text">The attacking player (No. 10) attempts to kick the ball beyond the opposing team's goalkeeper, between the goalposts, and beneath the crossbar to score a goal.</div>
                            <div class="text">Association football, more commonly known as football or soccer,[a] is a team sport played between two teams of eleven players with a spherical ball. It is played by 250 million players in over 200 countries and dependencies, making it the world's most popular sport.[5][6][7][8] The game is played on a rectangular field with a goal at each end. The object of the game is to score by moving the ball beyond the goal line into the opposing goal. The earliest form of football can be traced back to China (Han dynasty), originating from cuju, an ancient Chinese football game with standardized games and established rules, as recognized by FIFA.

Players are not allowed to touch the ball with outstretched hands or arms while it is in play, unless they are goalkeepers within their penalty area. Other players mainly use their feet to strike or pass the ball, but may also use any other part of their body except the hands and the arms. The team that scores the most goals by the end of the match wins. If the score is level at the end of the game, either a draw is declared or the game goes into extra time or a penalty shootout depending on the format of the competition. The Laws of the Game were originally codified in England by The Football Association in 1863. Association football is governed internationally by the International Federation of Association Football (FIFA; French: Fédération Internationale de Football Association), which organises World Cups for both men and women every four years</div>
<br>
                             <h2>Our Challanging Part</h2>
                                    <div class="text">
                                        <p>The rules of association football were codified in England by the Football Association in 1863 and the name association football was coined to distinguish the game from the other forms of football played at the time, specifically rugby football. The first written "reference to the inflated ball used in the game" was in the mid-14th century: "Þe heued fro þe body went, Als it were a foteballe".[12] The Online Etymology Dictionary states that the word "soccer" was "split off in 1863".[13] According to Partha Mazumdar, the term soccer originated in England, first appearing in the 1880s as an Oxford "-er" abbreviation of the word "association".[14]

Within the English-speaking world, association football is now usually called football in the United Kingdom and mainly soccer in Canada and the United States. People in Australia, Ireland, South Africa and New Zealand use either or both terms, although national associations in Australia and New Zealand now primarily use "football" for the formal name.[15]

</p>
                                    </div>
                        </div>
                        
                      
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    
  
    
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>