<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Blogs Detail </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Blogs </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li  class=""><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>
                                     <li><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li><a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                     
  <?php
include('db.php');

$var=$_GET['q'];

$result = mysqli_query($con,"SELECT * FROM blogs WHERE id=$var");
while($row = mysqli_fetch_array($result))
{

echo '



 
                        <div class="service-images">
                            <div class="row clearfix">
                                <div class="column col-md-12 ">
                                    <figure class="image">
                                        <a href="images/resource/service-detail-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="admin/photos/blogs/'.$row['img'].'" alt="" /></a>
                                    </figure>
                                    
                                </div>
                               
                             </div>
                        </div>







  <div style="text-align:right" class="bold-text">'.$row['datee'].'</div>
                        

                        <div class="text-content">
                            <h2>'.$row['name'].' </h2>
                            <div class="bold-text">'.$row['description'].'</div>
                        
';
}
?>

                                  
                        </div>
                        
                      
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    
    <!--subscribe-style-one-->
    <section class="subscribe-style-one" style="background-image:url(images/background/subscriber-bg.jpg);">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="col-md-12 col-sm-12">
                	<h2>"Better Coach" "Better Player" "Better Game" !!</h2>
                   
                </div>
               
            </div>
        </div>
    </section>
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>