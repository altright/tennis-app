<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Yoga </h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Sports Category </li>
                </ul>
            </div>
            
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
    	<!--Tabs Box-->
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Sidebar-->      
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        <!--Sidebar Widget / Styled Nav-->
                        <div class="widget sidebar-widget styled-nav">
                            <nav class="nav-outer">
                                <ul>
                                    <li><a href="allcategory.php"><span class="icon fa flaticon-play-button-3"></span>See all Category</a></li>
                                    <li><a href="tennis.php"><span class="icon fa flaticon-play-button-3"></span>Lawn Tennis</a></li>
                                    <li  ><a href="football.php"><span class="icon fa flaticon-play-button-3"></span>Football</a></li>
                                    <li ><a href="kabbadi.php"><span class="icon fa flaticon-play-button-3"></span>Kabbadi</a></li>
                                    <li ><a href="cricket.php"><span class="icon fa flaticon-play-button-3"></span>Cricket</a></li>
                                    <li ><a href="athelete.php"><span class="icon fa flaticon-play-button-3"></span>Athelete</a></li>
                                    <li ><a href="basketball.php"><span class="icon fa flaticon-play-button-3"></span>Basket Ball</a></li>
                                     <li><a href="skating.php"><span class="icon fa flaticon-play-button-3"></span>Skating</a></li>
                                    <li  > <a href="swimming.php"><span class="icon fa flaticon-play-button-3"></span>Swimming</a></li>
                                    <li class="current"><a href="yoga.php"><span class="icon fa flaticon-play-button-3"></span>Yoga</a></li>


                                </ul>
                            </nav>
                        </div>
                        
                        <!--Sidebar Widget / Contact Widget-->
                        <div class="widget sidebar-widget contact-widget" style="background-image:url(images/resource/call-us-bg.jpg);">
                        	<div class="inner">
                                <h2>Call us Now</h2>
                                <div class="text">For Inqury</div>
                                <div class="phone-numbers">
                                   +91 88 9841 5767 
                                </div>
                            </div>
                        </div>
                        
                    </aside>
                </div>
                <!--End Sidebar-->
                
                <!--Content Side-->      
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Single Service-->
                    <section class="services-single">
                            	
                        <!--Default-section-one-->
                        <div class="service-images">
                        	<div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <figure class="image">
                                        <a href="images/yoga1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/yoga1.jpg" alt="" /></a>
                                    </figure>
                                    
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                    <figure class="image">
                                        <a href="images/yoga.png" class="lightbox-image" title="Image Caption Here"><img src="images/yoga.png"  alt="" /></a>
                                    </figure>
                                    <figure class="image">
                                        <a href="images/yoga2.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/yoga2.jpg" alt="" /></a>
                                    </figure>
                                </div>
                             </div>
                        </div>
                        
                        <!--text-content-->
                        <div class="text-content">
                        	<h2>Yoga </h2>
                            <div class="bold-text">Yoga is a group of physical, mental, and spiritual practices or disciplines which originated in ancient India. There is a broad variety of yoga schools, practices, and goals in Hinduism, Buddhism, and Jainism.</div>
                            <div class="text">Yoga as described in the Yoga Sutras of Patanjali refers to Ashtanga yoga.[40] The Yoga Sutras of Patanjali is considered as a central text of the Yoga school of Hindu philosophy,[45] It is often called "Rāja yoga", "yoga of the kings," a term which originally referred to the ultimate, royal goal of yoga, which is usually samadhi,[39] but was popularised by Vivekananda as the common name for Ashtanga Yoga.[40]

Ashtanga yoga incorporates epistemology, metaphysics, ethical practices, systematic exercises and self-development techniques for body, mind and spirit.[46] Its epistemology (pramanas) is same as the Samkhya school. Both accept three reliable means to knowledge – perception (pratyākṣa, direct sensory observations), inference (anumāna) and testimony of trustworthy experts (sabda, agama). Both these orthodox schools are also strongly dualistic. Unlike the Sāṃkhya school of Hinduism, which pursues a non-theistic/atheistic rationalist approach,[47][48] the Yoga school of Hinduism accepts the concept of a "personal, yet essentially inactive, deity" or "personal god".[49][50] Along with its epistemology and metaphysical foundations, the Yoga school of Hindu philosophy incorporates ethical precepts (yamas and niyamas) and an introspective way of life focused on perfecting one's self physically, mentally and spiritually, with the ultimate goal being kaivalya (liberated, unified, content state of existence).

</div>

                        </div>
                        
                      
                    </section>
                
                </div><!--End Content Side-->
                
            </div>
        </div>
    </div>
    
  
    
  
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>