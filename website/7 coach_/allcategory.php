<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Our Sports Category</h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Our Sports Category</li>
                </ul>
            </div>
            
        </div>
    </section>
    
    <!--News Section-->
    <section class="news-section">
    	<div class="auto-container">
                
        	<div class="row clearfix">
            
            	<!--News Style One-->
            	<div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="football.php"><img src="images/gallery/a1.jpg" alt=""></a>
                       </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="football.php">Football</a></h3>
                            <div class="text">In the last few years, the popularity of ‘turf football’ has spiked dramatically and at PSA, we are committed to ensuring that all of our members constantly have access to the best..</div>
                            <a href="football.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--News Style One-->
            	<div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="basketball.php"><img src="images/gallery/a2.jpg" alt=""></a>
                       </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="basketball.php">Basket Ball</a></h3>
                            <div class="text">
“If all I'm remembered for is being a good basketball player, then I've done a bad job with the rest of my life.” </div>
                            <a href="basketball.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--News Style One-->
            	<div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
           <figure class="image-box"><a href="tennis.php"><img src="images/gallery/a3.jpg" alt=""></a>
          </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="tennis.php">Lawn Tennis</a></h3>
                            <div class="text">Tennis coaching is also offered in the evenings and interested parties are encouraged to inquire with the club about the same.The club provides rackets and balls for member’s convenience.</div>
                            <a href="tennis.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--News Style One-->
            	<div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="kabaddi.php"><img src="images/gallery/a4.jpg" alt=""></a>
                       </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="kabaddi.php">Kabaddi</a></h3>
                            <div class="text">I am a sports enthusiast, and if given an opportunity, I want to be a sportsman, even today. I want to promote the sport that is indigenous to India.  </div>
                            <a href="kabaddi.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--News Style One-->
            	<div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="cricket.php"><img src="images/gallery/a5.jpg" alt=""></a>
                       </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="cricket.php">Cricket</a></h3>
                            <div class="text">The PSA Cricket Club has a cricket ground that has not only seen cricketing history happen, but is the pivot for professional cricketing action amongst.. </div>
                            <a href="cricket.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                
                <!--News Style One-->
            	<div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="athelete.php"><img src="images/gallery/a6.jpg" alt=""></a>
                     </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="athelete.php">Athelete</a></h3>
                            <div class="text">Winning is great, sure, but if you are really going to do something in life, the secret is learning how to lose. Nobody goes undefeated all the time.  </div>
                            <a href="athelete.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>












                   <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="athelete.php"><img src="images/skating.jpg" alt=""></a>
                     </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="skating.php">Skating</a></h3>
                            <div class="text">Ice skating is the act of motion by wearer of the ice skates to propel the participant across a sheet of ice. This can be done for a variety of reasons, including exercise, leisure, traveling, and various sports.</div>
                            <a href="skating.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                   <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="skating.php"><img src="images/swim.jpg" alt=""></a>
                     </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="swimming.php">Swimming</a></h3>
                            <div class="text">Swimming is an individual or team sport that requires the use of ones arms and legs to move the body through water. The sport takes place in pools or open water.</div>
                            <a href="swimming.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
                   <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href="swimming.php"><img src="images/yoga.png" alt=""></a>
                     </figure>
                        <div class="posted">
                          
                        </div>
                        <div class="content">
                            <h3><a href="yoga.php">Yoga</a></h3>
                            <div class="text">Yoga is a group of physical, mental, and spiritual practices or disciplines which originated in ancient India. There is a broad variety of yoga schools, practices, and goals in Hinduism, Buddhism, and Jainism.</div>
                            <a href="yoga.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                        </div>
                    </div>
                </div>
               
            </div>
            
           
        </div>
    </section>
    

    
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>