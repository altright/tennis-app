   <!--Main Footer-->
    <footer class="main-footer">
    	
        <!--Footer Upper-->        
        <div class="footer-upper">
            <div class="auto-container">
                <div class="row clearfix">
                
                	<div class="col-md-6 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                        
                            <div class="col-md-6 col-sm-6 col-xs-12 column">
                                <div class="footer-widget about-widget">
                                    <h2>About Us</h2>
                                    <div class="text">
                                        <p>Our Association- Promote the game from School Level to Elite Level
Create a robust Tournament Structure for Junior to Senior Level Players
Conduct State, National and International Level Tournaments. We Create more infrastructure in india to spread the game and competition. 
</p>
                                    </div>
                                    <ul class="footer-social">
                                        <li><a href="https://www.facebook.com/Krishna-Tennis-Academy-Mumbai-454289058250294/?hc_ref=ARSNtqQv-o5Ey5OWLWLgx9wk005qP5vEuX5IQmFvxE0ZEm9Guvd3d1rU76H_coSGbDw&fref=nf"><span class="fa fa-facebook-f"></span></a></li>
                                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                      
                                    </ul>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12 column">
                                <div class="footer-widget links-widget">
                                <h2>Quick Links</h2>
                                    <ul>  <li><a href="index.php">Home</a>  </li>
                             <li ><a href="about-us.php">About Us</a>
                                        <li><a href="photo.php">Photo Gallery</a></li>
                                        <li><a href="video.php">Video Gallery</a></li>
                                        <li><a href="news.php">News & Events</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                            <!--Footer Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12 column">
                                <div class="footer-widget links-widget">
                                <h2>Sports Category</h2>
                                    <ul>
                                       <li><a href="tennis.php">Lawn Tennis</a></li>
                                        <li><a href="football.php">Football</a></li>
                                         <li><a href="kabbadi.php">Kabbadi</a></li>
                                        <li><a href="cricket.php">Cricket</a></li>
                                         <li><a href="athelete.php">Athelete</a></li>
                                        <li><a href="basketball.php">Basket Ball</a></li>

                                          <li><a href="skating.php">Skating</a></li>
                                         <li><a href="swimming.php">Swimming</a></li>
                                        <li><a href="yoga.php">Yoga</a></li>
                                    </ul>
        
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12 column">
                                <div class="footer-widget">
                                    <h2>Get In Touch</h2>	
                                    <ul class="contact-info">
                                        <li><span class="flaticon-placeholder-1"></span>Dadoji Kondev Ground Opp,
Vishnu Shivam Mall, Behind Victory Court,
Thakur Village Kandivali [E] Mumbai - 4001 01.
</li>
                                        <li><span class="flaticon-technology-1"></span>+91 88 9841 5767  <br>
+91 70 4552 5557</li>
                                        <li><span class="flaticon-email-envelope-outline-shape-with-rounded-corners"></span>krishnatennis9@gmail.com</li>
                                        
                                    </ul>
                                </div>
                            </div>
                       	</div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
        <!--Footer Bottom-->
    	<div class="footer-bottom">
            <div class="auto-container">
                <div class="outer-box clearfix">
                    <!--Copyright-->
                	<div>
                    	<div class="copyright">Copyright 2016 <a href="http://siddhivinayakcreativewitty.com/" target="_blank">Siddi Vinayak Creativewitty </a> &copy; 2018</div>
                    </div>
                 </div>
            </div>
        </div>
        
    </footer>