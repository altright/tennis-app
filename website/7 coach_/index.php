<?php 
include "allcss.php";
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
    <div class="page-wrapper">

        <!-- Preloader -->
        <div class="preloader"></div>

        <!--Main Header-->
        <header class="main-header">

            <!-- Header Top -->
            <div style="background: #222222;" class="header-top">
                <div class="auto-container clearfix">
                    <!--Top Left-->  <div class="top-left pull-left">
                        <ul class="links-nav clearfix">
                         <li  style="
    background-color: #de0000;padding:10px;
     color: white; 
"> <a style="color: white;" href="admission.php">Admission Form</a> </li>
                            
                        </ul>
                    </div>
                    <div class="top-left pull-right">
                        <ul class="links-nav clearfix">
                       
                            <li style="color: #ffffff;">Welcome to <a href="#">Play Sports Association</a> biggest Coaching Center !!</li>
                        </ul>
                    </div>

                </div>
            </div>
            <!-- Header Top End -->

            <!--Header-Upper-->
            <div class="header-upper">
                <div class="auto-container">
                    <div class="clearfix">

                        <div class="pull-left logo-outer">
                            <div class="logo">
                                <a href="index.php"><img src="images/logo.png" width="320px" alt="Gardner" title="Gardner"></a>
                            </div>
                        </div>

                        <div class="pull-right upper-right clearfix">

                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="icon-box"><span class="flaticon-placeholder-1"></span></div>
                                <ul>

                                    <li><strong>Dadoji Kondev Ground Opp,Vishnu Shivam Mall,</strong></li>
                                   
                                    <li> Thakur Village Kandivali [E] Mumbai - 4001 01.</li>
                                     <li>+91 88 9841 5767 / +91 70 4552 5557</li>
                                </ul>
                            </div>

                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="social-links-one">
                                    <a target='_blank' href="https://www.facebook.com/Krishna-Tennis-Academy-Mumbai-454289058250294/?hc_ref=ARSNtqQv-o5Ey5OWLWLgx9wk005qP5vEuX5IQmFvxE0ZEm9Guvd3d1rU76H_coSGbDw&fref=nf"><span class="fa fa-facebook-f"></span></a>
                                    <a href="#"><span class="fa fa-twitter"></span></a>
                                    <a href="#"><span class="fa fa-google-plus"></span></a>
                                    
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <!--Header-Lower-->
            <div class="header-lower">
                <!--Background Layer-->
                <div class="bg-layer"></div>

                <div class="auto-container">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="index.php">Home</a>

                                    </li>
                                    <li><a href="about-us.php">About Us</a>
                                        <!--<ul>

                                        <li><a href="objective.php">Our Objective</a></li>
                                         <li><a href="vision.php">Vision & Mission</a></li>
                                        <li><a href="bm.php">Board Members</a></li>
                                         <li><a href="coach.php">Coach Names</a></li>
                                    </ul> -->
                                    </li>
                                    <li class="dropdown"><a href="category.php">Sports Category</a>
                                        <ul>
                                            <li><a href="tennis.php">Lawn Tennis</a></li>
                                            <li><a href="football.php">Football</a></li>
                                            <li><a href="kabbadi.php">Kabbadi</a></li>
                                            <li><a href="cricket.php">Cricket</a></li>
                                            <li><a href="athelete.php">Athelete</a></li>
                                            <li><a href="basketball.php">Basket Ball</a></li>
                                              <li><a href="skating.php">Skating</a></li>
                                         <li><a href="swimming.php">Swimming</a></li>
                                        <li><a href="yoga.php">Yoga</a></li>
                                        
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="gallery.php">Gallery</a>
                                        <ul>
                                            <li><a href="photo.php">Photo Gallery</a></li>
                                            <li><a href="video.php">Video Gallery</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="news.php">News & Events</a>

                                    </li> <li><a href="blogs.php">Blogs</a>
                                   
                                </li>

                                    <li><a href="contact.php">Contact us</a>

                                    </li>

                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        <div class="btn-outer"><a href="booking.php" class="theme-btn quote-btn"><span class="border"></span>Booking</a></div>

                    </div>
                </div>
            </div>

            <?php
include "stickyheader.php";
        ?>

        </header>
        <!--End Main Header -->

        <!--Main Slider-->
        <section class="main-slider negative-margin">

            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>



   <?php

                                               include "db.php";

$result = mysqli_query($con,"SELECT * FROM slider");
while($row = mysqli_fetch_array($result))
{

echo '
  <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/1.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                            <img src="admin/photos/slider/'.$row['img'].'" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                        </li>                               

';
}
?>

                    </ul>

                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </section>

        <!--call-to-action-->
        <section class="call-to-action">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="column col-md-9 col-sm-12">
                        <!--call-action-iner-->
                        <div class="call-action-iner">
                            <p>We Create more infrastructure <span>in india </span> to spread the game and competition.</p>
                        </div>
                    </div>
                    <div class="column col-md-3 col-sm-12 text-right">
                        <!--call-action-button-->
                        <div class="call-action-button">
                            <a href="allcategory.php">Check Service</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--default-section-->
        <section class="default-section">
            <div class="auto-container">
                <div class="row clearfix">

                    <div class="column col-md-6 col-sm-12 col-xs-12">
                        <!--who-we-are-->
                        <div class="who-we-are">




                        




                            <h2> About the <span>Play Sports Association</span></h2>
                            <p>Play sports association is registered under the societies registration act 862/2018.</p>

                            <div style="padding-bottom:10px" class="provide wow slideInLeft" data-wow-duration="1500ms" data-wow-delay="0ms" >
                              
                                 <div class="service-block-one">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="row clearfix">
                                        <div class="image-column col-md-5 col-sm-5 col-xs-12">
                                            <figure class="image-box">
                                                <a href="football.php"><img src="images/mla.jpg" alt=""></a>
                                               
                                            </figure>
                                              
                                        </div>

                                        <div class="content-column col-md-7 col-sm-7 col-xs-12">
                                            <div class="content">
                                             
                                                <div class="text"><img width="25px" src="images/start.png" > &nbsp;&nbsp;&nbsp;Play sports association's approach to spread awareness about physical education for different sports is phenomenal. Play sports association is doing a good job by promoting the game of sports professionally .
Their training sessions are of professional standard. Their vision to produce the future stars of sports for India is what I see becoming a reality soon.
I wish them all the very best and luck in their endeavours.&nbsp;&nbsp;&nbsp;<img width="25px" src="images/end.png" ></div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 style="line-height: 1em;"><a style="color:white" href=""><br> Thakur Ramesh Singh.  </a></h3>
                                <h5 style="line-height: 1em;"><a style="color:white" href=""><br> ( Trustee, Thakur Educational Trust )  </a></h5>
                            </div>


                            </div>


                              <!--Service Block-->
                         

                            <div class="text-content">
                                <p>
we are looking to create awareness for sports and fitness as an important part of moral and physical education.Since the last 7 years we have gained expertise in providing physical education for schools in India, sports training, school sports management and total sports management services for schools and colleges. Adding to that we also conduct Training Programs for all outdoor and indoor sports.</p>
                                <p>This will give the schools and colleges an exposure to sports and help them reach new heights..</p>
                            </div>
                            <a href="about-us.php" class="theme-btn">Learn More About Us <span class="flaticon-play-button-3"></span></a>
                        </div>
                    </div>

                    <div class="column col-md-6 col-sm-12 col-xs-12">
                        <!--Service Style One-->
                        <div class="service-style-one">

                            <!--Service Block-->
                            <div class="service-block-one">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="row clearfix">
                                        <div class="image-column col-md-5 col-sm-5 col-xs-12">
                                            <figure class="image-box">
                                                <a href="football.php"><img src="images/home/h1.jpg" alt=""></a>
                                            </figure>
                                        </div>

                                        <div class="content-column col-md-7 col-sm-7 col-xs-12">
                                            <div class="content">
                                                <h3><a href="football.php">Football</a></h3>
                                                <div class="text">In the last few years, the popularity of ‘turf football’ has spiked dramatically and at PSA, we are committed to ensuring that all of our members constantly have access to the best.</div>
                                                <a href="football.php" class="theme-btn read-more">Read More <span class="fa fa-life-bouy"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Service Block-->
                            <div class="service-block-one">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="row clearfix">
                                        <div class="image-column col-md-5 col-sm-5 col-xs-12">
                                            <figure class="image-box">
                                                <a href="athelete.php"><img src="images/home/h2.jpg" alt=""></a>
                                            </figure>
                                        </div>

                                        <div class="content-column col-md-7 col-sm-7 col-xs-12">
                                            <div class="content">
                                                <h3><a href="athelete.php">Athelete</a></h3>
                                                <div class="text">An athlete is a person who competes in one or more sports that involve physical strength, speed or endurance. The application of the term to those who participate in other activities, such as horse riding or driving, is somewhat controversial.</div>
                                                <a href="athelete.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Service Block-->
                            <div class="service-block-one">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="row clearfix">
                                        <div class="image-column col-md-5 col-sm-5 col-xs-12">
                                            <figure class="image-box">
                                                <a href="kabbadi.php"><img src="images/home/h3.jpg" alt=""></a>
                                            </figure>
                                        </div>

                                        <div class="content-column col-md-7 col-sm-7 col-xs-12">
                                            <div class="content">
                                                <h3><a href="kabbadi.php">Kabaddi</a></h3>
                                                <div class="text">Kabaddi is a contact team sport.The game is said to have had its inception in the southern state of Tamil Nadu, India. The word ‘kabaddi’ owes its origin to the Tamil word, ‘kai-pidi’, which means holding hands.</div>
                                                <a href="kabbadi.php" class="theme-btn read-more">Read More <span class="fa flaticon-play-button-3"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!--service-style-two-->
        <section class="service-style-two">
            <div class="auto-container">
                <div class="sec-title centered">
                    <h2>Our <span>Sports Category</span></h2>
                </div>

                <div class="row">
                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-life-bouy"></span>
                            </div>
                            <h2>Football</h2>
                            <div class="text">The supreme quality for leadership is unquestionably integrity. Without it, no real success is possible, no matter whether it is on a section gang, a football field, in an army, or in an office.</div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-linode"></span>
                            </div>
                            <h2>Basket Ball</h2>
                            <div class="text">“The first time I managed to pick up a basketball I knew I was destined to lead the UK to another National championship. ... Even now, so many years later, I still believe Kentucky will go undefeated in March & win everything.” </div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-mars-double"></span>
                            </div>
                            <h2>Lawn Tennis</h2>
                            <div class="text">Tennis coaching is also offered in the evenings and interested parties are encouraged to inquire with the club about the same.The club provides rackets and balls for member’s convenience.</div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-users"></span>
                            </div>
                            <h2>Kabaddi</h2>
                            <div class="text">Rules of "life" are same as that of the game "kabaddi"..
The moment you "touch" your "success" line,
"People" will start pulling your "leg".!


</div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                              <span class="fa fa-angellist"></span>    
                            </div>
                            <h2>Cricket</h2>
                            <div class="text">The PSA Cricket Club has a cricket ground that has not only seen cricketing history happen, but is the pivot for professional cricketing action amongst the youngsters in suburban Mumbai. </div>
                        </div>
                    </div>

                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-block">
                            <div class="icon-box">
                                <span class="fa fa-viacoin"></span>
                            </div>
                            <h2>Athelete</h2>
                            <div class="text">Don't walk through life just playing football. Don't walk through life just being an athlete. Athletics will fade. Character and integrity and really making an impact on someone's life, that's the ultimate vision, that's the ultimate goal - bottom line.</div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <!--call-to-acton-two-->
        <section class="call-to-acton-two" style="background-image:url(images/background/call-action-two.jpg);">
            <div class="auto-container">
                <div class="text-center">
                    <div class="inner">
                        <h2>“The key is not the will to win… everybody has that. It is the will to prepare to win that is important.”</h2>
                        <a class="theme-btn btn-style-four" href="allcategory.php">See service</a>
                        <a class="theme-btn btn-style-five" href="contact.php">Contact Us</a>
                    </div>
                </div>
            </div>
        </section>

        <!--FAQs Style One-->
        <section class="faq-style-one">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <figure class="image-box"><img src="images/home/h5.jpg" alt="" /></figure>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 questions">
                        <div class="sec-title-two">
                            <h2>Mission &amp; <span>Vision</span></h2>

                            <ul class="accordion-box">

                                <!--Block-->
                                <li class="accordion block">
                                    <div class="acc-btn active">
                                        <div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div> Our Mission</div>
                                    <div class="acc-content current">
                                        <div class="content">
 <p>To become the most preferred service provider for school PE and SPORTS programs in India. <br>
   <b> <span class="glyphicon glyphicon-arrow-right"></span> </b> To help children pursue  sports as a career.<br>
<b> <span class="glyphicon glyphicon-arrow-right"></span> </b> To create awareness about physical fitness through sports to people of all age groups.<br>
<b> <span class="glyphicon glyphicon-arrow-right"></span> </b> Our Association- to work on the overall development of kids leading to complete personality development Our approach: Overall development of kids through experimental learning.</p>

                                        </div>
                                    </div>
                                </li>

                                <!--Block-->
                                <li class="accordion block">
                                    <div class="acc-btn">
                                        <div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div>Our Vision</div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <p><b> <span class="glyphicon glyphicon-arrow-right"></span> </b> Create players to represent the country and win medals in all Team Championships. 
 <br><b> <span class="glyphicon glyphicon-arrow-right"></span> </b> Create more infrastructure in india to spread the game and competition
<br><b> <span class="glyphicon glyphicon-arrow-right"></span> </b> Build discipline, self confidence, leadership and decision making skills
 <br><b> <span class="glyphicon glyphicon-arrow-right"></span> </b> Help children understand importance of physical fitness

</p>
                                        </div>
                                    </div>
                                </li>


                                  <!--Block-->
                                <li class="accordion block">
                                    <div class="acc-btn">
                                        <div class="icon-outer"><span class="icon icon-plus fa fa-plus"></span> <span class="icon icon-minus fa fa-minus"></span></div>Our Objective</div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <p><b> <span class="glyphicon glyphicon-arrow-right"></span> </b> Regularly arranging a present meet to show the progress of their child .<br>
<b> <span class="glyphicon glyphicon-arrow-right"></span> </b> selecting the best players and giving them advance coaching so that they can represent the school, club  at State and national levels.<br>
<b> <span class="glyphicon glyphicon-arrow-right"></span> </b> coaching will not only include the how to play aspects but  also the ' who's who ' of the game staying game plans of existing teams and their players.<br>
<b> <span class="glyphicon glyphicon-arrow-right"></span> </b>Promote the game from School Level to Elite Level.
Create a robust Tournament Structure for Junior to Senior Level Players
Conduct State, National Level Tournaments.</p>
                                        </div>
                                    </div>
                                </li>




                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </section>











         <!--team-section-->
    <section class="team-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our <span>Promoters</span></h2>
            </div>
        
            <div class="row clearfix">
               



   <?php

                                               include "db.php";

$result = mysqli_query($con,"SELECT * FROM promoters limit 4");
while($row = mysqli_fetch_array($result))
{

echo '
  <div class="col-md-3 col-sm-6 col-xs-12 column team-member-one">
                    <div   class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                       <img src="admin/photos/promoters/'.$row['img'].'" alt="" />
                            
                          
                            
                       
                      
                    </div>
                </div>                          

';
}
?>



              
                
                           </div>
        </div>
    </section>
    





         <!--team-section-->
    <section class="team-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our <span>Partners</span></h2>
            </div>
        
            <div class="row clearfix">
               


 <?php

                                               include "db.php";

$result = mysqli_query($con,"SELECT * FROM partners limit 4");
while($row = mysqli_fetch_array($result))
{

echo '
  <div class="col-md-3 col-sm-6 col-xs-12 column team-member-one">
                    <div   class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                       <img src="admin/photos/partners/'.$row['img'].'" alt="" />
                            
                          
                            
                       
                      
                    </div>
                </div>                          

';
}
?>



              
                
                           </div>
        </div>
    </section>


        <!--Map Section-->
        <section class="map-section">
            <div class="map-outer">

                <div style="overflow:hidden;resize:none;max-width:100%;width:100%;height:350px;">
                    <div id="mymap-display" style="height:100%; width:100%;max-width:100%;">
                        <iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Dadoji+Kondev+Ground+Opp,+Vishnu+Shivam+Mall,+Behind+Victory+Court,+Thakur+Village+Kandivali+[E]+Mumbai+-+4001+01.&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe>
                    </div><a class="googlemap-code" rel="nofollow" href="https://www.embed-map.com" id="grabmaps-authorization">https://www.embed-map.com</a>
                    <style>
                        #mymap-display img {
                            max-height: none;
                            max-width: none!important;
                            background: none!important;
                        }
                    </style>
                </div>

            </div>
        </section>











        <?php
include "footer.php"
?>

            <?php
include "allscript.php";
  ?>