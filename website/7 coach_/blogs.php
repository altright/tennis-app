<?php
include "allcss.php";
?>
<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
<?php
include "header.php";
?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/page-title-1.jpg);">
        <div class="auto-container">
            <h1>Blogs</h1>
            
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="index-2.html">Home</a></li>
                    <li class="active">Our Blogs</li>
                </ul>
            </div>
            
        </div>
    </section>
    
    <!--News Section-->
    <section class="news-section">
        <div class="auto-container">
                
            <div class="row clearfix">
            




   <?php

                                               include "db.php";

$result = mysqli_query($con,"SELECT * FROM blogs");
while($row = mysqli_fetch_array($result))
{

echo '
 
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12 col-xs-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image-box"><a href=".html"><img src="admin/photos/blogs/'.$row['img'].'" alt=""></a><div class="date"><span class="day">16</span>July</div></figure>
                        <div class="posted">
                            Posted on <span>'.$row['datee'].'</span>
                        </div>
                        <div class="content">
                            <h3><a href=".html">'.$row['name'].'</a></h3>
                            <div class="text">'.$row['description'].'</div>
                            
                             <a  href="blogsdetail.php?q='.$row['id'].'" class="theme-btn read-more">Read More<span class="fa flaticon-play-button-3"></span></a>


                          
                        </div>
                    </div>
                </div>                            

';
}
?>
                
                
              
                
                
            </div>
            
                   </div>
    </section>
     <!--subscribe-style-one-->
    <section class="subscribe-style-one" style="background-image:url(images/background/subscriber-bg.jpg);">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="col-md-12 col-sm-12">
                	<center><h2>"Better Coach" "Better Player" "Better Game" !!</h2></center>
                   
                </div>
               
            </div>
        </div>
    </section>
 <?php 
include "footer.php";
 ?>
    <?php
include "allscript.php";
    ?>